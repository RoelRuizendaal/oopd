int WIT = 255;
int ZWART = 0;

Damsteen[] damstenen;

void setup() {
   size(450,450);
   background(100,100,100);
   
   damstenen = new Damsteen[4];
   damstenen[0] = new Damsteen(30,30,40,WIT, ZWART);
   damstenen[1] = new Damsteen(30,100,40,WIT, ZWART);
   damstenen[2] = new Damsteen(100,30,40,ZWART, WIT);
   damstenen[3] = new Damsteen(100,100,40,ZWART, WIT);
   damstenen[3].toggle();
}

void draw() {
  background(100,100,100);
  
  for(int i = 0; i < damstenen.length; i++) {
    tekenDamsteen(damstenen[i]);
  }
}

void tekenDamsteen(Damsteen damsteen) {
  if(damsteen.selected) {
    stroke(damsteen.klikKleur);
  } else {
    noStroke();
  }
  
  fill(damsteen.kleur);
  ellipse(damsteen.x, damsteen.y, damsteen.diameter, damsteen.diameter);
}

void mouseClicked() {
  for(int i = 0; i < damstenen.length; i++) {
    if(mouseHoveringOver(damstenen[i])) {
      damstenen[i].toggle();
    } else {
       damstenen[i].deselect(); 
    }
  }
}

boolean mouseHoveringOver(Damsteen damsteen) {
   double distance = distanceBetween(damsteen.x,damsteen.y, mouseX, mouseY);
   return distance < (damsteen.diameter / 2);
}

double distanceBetween(int x1, int y1, int x2, int y2) {
  int xDiff = x1 - x2;
  int yDiff = y1 - y2;
  double distance = sqrt((xDiff*xDiff)+(yDiff*yDiff));
  return distance;
}
