int[] lijst1 = {
  1, 2, 3, 4, 5, 6, 7, 8, 9, 10
};
int[] lijst2 = {
  3, 2, 6, 1, 8, 5, 10, 9, 7, 4
};
int[] lijst3 = { 
  1, 2, 3
};

void setup() {
  println(maakMaxArray(lijst2, lijst3));
}

//Telt twee arrays op en returned een array met het resultaat
int[] telElementenOp(int[] getallen1, int[] getallen2) {
  int[] resultaat = new int[getallen1.length];
  for (int i=0; i<getallen1.length; i++) {
    resultaat[i]=getallen1[i]+getallen2[i];
  }
  return resultaat;
}

//Creëert een array met de max waarden van 2 ingevoerde arrays.
int[] maakMaxArray(int[] array1, int[] array2) {
  int[] max;
  if (array1.length > array2.length) {
    max = new int[array1.length];
  } 
  else {
    max = new int[array2.length];
  }  
  for (int i=0; i < max.length; i++) {
    if (i >= array1.length) {
      max[i] = array2[i];
    } 
    else if ( i >= array2.length) {
      max[i] = array1[i];
    } 
    else {
      if (array1[i] >= array2[i]) {      
        max[i] = array1[i];
      } 
      else {
        max[i] = array2[i];
      }
    }
  }
  return max;
}



