class Damsteen {
 int x, y, diameter, kleur, klikKleur;
 boolean selected;
 
 Damsteen(int x, int y, int diameter, int kleur, int klikKleur) {
   this.x = x;
   this.y = y;
   this.diameter = diameter;
   this.kleur = kleur;
   this.klikKleur = klikKleur;
   this.selected = false;
 }
 
 void toggle() {
   selected = !selected;
 }
 
 void deselect() {
   selected = false; 
 }
}
