/**Auteur: Roel Ruizendaal

  *Opgave 1 week 1 lesvoorbereiding OOPD

  *Declareren en direct initialiseren van de op te tellen arrays
  */
int[] array1 = {1,2,3,4,5};
int[] array2 = {6,7,8,9,10};


/**Setup wordt in processing gezien als de entrypoint.
  *Setup roept telElementenOp() aan, waar de arrays hierboven
  *worden meegegeven als parameters, en het resultaat wordt
  *uitgeprint
  */
void setup(){
  print(telEelementenOp(array1, array2));
}

/**Het resultaat wordt verhoogd met zichzelf + het resultaat
  *van de twee elementen op zijn huidige index bij elkaar opgeteld.
  *Hierna wordt het resultaat teruggeven.
  */
  
int telEelementenOp(int[] array1, int[] array2){
  int som = 0;
  for(int i = 0; i < array1.length; i++){
    som = som + array1[i] + array2[i];
  }
  return som;
}
    
